一个谷歌搜索小书签：

```
JavaScript:var i_name=prompt('请输入要搜索的关键字','');if(i_name){window.location.href='https://www.google.com/search?q='+i_name;}
```

好处：可以在弹窗中输入搜索关键字而不用先联网进入搜索页面再输入。
可以自行修改单引号内的搜索引擎'https://....q='

谷歌：https://www.google.com/search?q=

duckduckgo：https://duckduckgo.com/?q=

搜索第三方谷歌商店apk：https://m.apkpure.com/search?q=